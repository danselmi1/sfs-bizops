# Account an SFS event

BEFORE CLASS:
* [ ] Confirm Meetup for number of students 

DURING CLASS
* [ ] Go through Meetup attendees for [SFS](https://www.meetup.com/sofreeus/)and [IT-NTL](https://www.meetup.com/it-ntl/) (if applicable) to mark people as show/noshow/paid

END OF CLASS: 
* [ ] Gather monies from each resource (give jar, PayPal, BitCoin, LiteCoin, 3rd parties)

1 DAY to 1 WEEK AFTER CLASS:

* [ ] Add new students to [sofree email list](http://lists.sofree.us/cgi-bin/mailman/admin/sfs/)
* [ ] Invoice 3rd parties (if applicable)
* [ ] Record A/R (all invoices get paid out as cash to the people below)
* [ ] Payout teacher (if applicable)  PAYPAL, CRYPTO, CASH, ECHECK
* [ ] Payout Upstream (if applicable) - DETERMINED BY TEACHER PAYPAL, CRYPTO, CASH, ECHECK
* [ ] Payout Location (if applicable and not part of expenses) PAYPAL, CRYPTO, CASH, ECHECK
* [ ] Payout Caterer (if applicable) PAYPAL, CRYPTO, CASH, ECHECK
* [ ] Payout VIP (if applicable) PAYPAL, CRYPTO, CASH, ECHECK
* [ ] Complete [financials] spreadsheet (in Nextcloud) 
* [ ] Send email to Board Members with financials
