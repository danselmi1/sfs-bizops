#### Post and Promote Checklist

Promote is merged into Post as of 2023-01-23

Consider using the [Event Template](event-template.md) to gather important information like teacher bio, preferred upstream, and so on.

- [ ] List the event on [Meetup](https://www.meetup.com/sofreeus/)
    * banner graphic
    * objectives
    * requirements
    * price
- [ ] "Announce" the Meetup event.
- [ ] at-channel in the [SFS Cafe](https://mattermost.sofree.us/sfs303/channels/town-square)
- [ ] email to sfs@lists.sofree.us
- [ ] Tweet from [SFS' Twitter account](https://twitter.com/sfs303)
- [ ] Post to [SFS on LinkedIn](https://www.linkedin.com/company/software-freedom-school/)

Optional:

Use [Contact Members](https://www.meetup.com/sofreeus/messages/send/) to notify Meetup members

Send personal invitations to individuals you think might be interested

Share meetup post on your own Mastodon, Linkedin, Facebook, Twitter accounts

Send an invite to groups where you are a member in good standing, the topic is appropriate, and the post will be welcome.
    - Other Meetups
    - Other Mailing Lists
    - Slack Channels

Ask others to help spread the word!
