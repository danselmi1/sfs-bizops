# Intro

## What are Gatherings?

The Gathering is our monthly meeting. It is also called Small Is Beautiful (though that's more of a format) and Software Freedom First Saturday. It occurs on the first Saturday of each month from 9AM to noon, usually at SFS HQ, which is also called The Comfy Room, or Casa de Willson.

### What do Gatherings consist of?

#### 2 Different Talks

* * *

- Each talk needs topic and teacher

- Tell the teachers: tune one talk as the intro talk - general overview

    * First presenter should be tuned for about a 22 min talk. 

- Then short break to clear the air, then play gathering track again to tell everyone to take seats.

- Second talk: can be talk and demo, or deep dive into a topic, or participatory. 

    * Length allows for almost anything, do not exceed 75min. (The "Time Pie" guideline gives ~15 minutes per section of talk)

  


# Planning a Gathering

## Finding presenters

Presenters can be found anywhere! 

Presenters can present a talk on anything as long as it is free and open-source aligned. This can be a discussion on the newest firmware for toaster ovens, as long as it meets [The four essential freedoms](https://www.gnu.org/philosophy/free-sw.en.html#four-freedoms).

Try and locate presenters a month in advance of each event, so if changes need to be made - we can accommodate.


# Get the Gathering Going

## Post and Promote Checklist

* * *

### Posting an event

If you know, dates, topics, and presenters... you can edit the event meetup to reflect this information.

- Any specifics that need updated may include:

  * banner graphic

  * objectives

  * requirements

  * price


* * *


### Promoting an event

These are different steps to be taken to help ensure the community is aware of then upcoming event.

- - [ ] "Announce" the Meetup event.

- - [ ] at-channel in the [SFS Cafe](https://mattermost.sofree.us/sfs303/channels/town-square)

- - [ ] email to sfs@lists.sofree.us

- - [ ] Tweet from [SFS' Twitter account](https://twitter.com/sfs303)

- - [ ] Post to [SFS on LinkedIn](https://www.linkedin.com/company/software-freedom-school/)

> Be sure if cross posting the event on any other relevant channels, users posting have access to login information as well.


* * *

**Optional:**

Use [Contact Members](https://www.meetup.com/sofreeus/messages/send/) to notify Meetup members

Send personal invitations to individuals you think might be interested

Share meetup post on your own Mastodon, Linkedin, Facebook, Twitter accounts

Send an invite to groups where you are a member in good standing, the topic is appropriate, and the post will be welcome.

- Other Meetups

- Other Mailing Lists

- Slack Channels

Ask others to help spread the word!
