# The SFS Rap

Welcome to the Software Freedom School. Our purpose here is to teach each other how to use and why to choose Free Software.

## Free Software Definition

What do we mean by "Free Software"?



> “Free software” means software that respects users' freedom and community. Roughly, it means that the users have the freedom to run, copy, distribute, study, change and improve the software. Thus, “free software” is a matter of liberty, not price. --[Richard Stallman](https://www.gnu.org/philosophy/free-sw.en.html)

## Why To Choose

It is important to be free to become a co-creator, not locked in a consumer role. It is important to leave others free to co-create, too.

If the making of software is art and science and progress is important and all progress is incremental, the preservation of creative freedom is important. We prefer to use and create software which respects the user's freedom to move from consumption to creation. We are complementarily biased *against* licenses that restrict users' freedom or worse, broad patents and copyrights that restrict the creative freedom of everyone, including innocent bystanders.

## Moral Consistency

SFS benefits from Free Software individually and corporately. It is important to us to act like our benefactors; not like GitHub, Apple, and other companies that benefit from Free Software but do not free their software. Everything SFS makes or causes to be made is free: our code is GPL and our other artistic works are CC BY SA.

## Facilities

  * Restrooms
  * Recycling

## Upcoming Events

[Upcoming Events](upcoming-events.md)

## Signing In

  * Mattermost or Big Blue Button Classroom
  * Sign-in link on screen and in the Mattermost/BBB Classrom

## Pricing and Payments

When we say 'free', we mean free as in freedom, not [lunch](https://en.wikipedia.org/wiki/There_ain%27t_no_such_thing_as_a_free_lunch). We work hard to provide enjoyable learning experiences, and we expect to be rewarded. We accept cash, checks, crypto-currencies, and PayPal.  If you would like to pay later, you may ask for an invoice or take an Envelope of Karmic Justice.

Today's class price is ($32, $64, $128, or whatever). If you'd rather pay more, less, or differently, you may Pay What You Choose.

We ALSO give these classes in a corporate wrapper.   We would loooove to give this same class to your company and even do some customization to suit your needs – if you like us and you like our class, please talk to us BEFORE you give any pricing information.   Our pricing is higher when we dress up, tell clean jokes and take time out DURING the week to teach and deliver ONSITE.   You CAN still get the same teachers, but keep in mind to come to us before presenting pricing to your decision maker.  

## Introduce the Delivery Team

  * Board-members
  * Other Volunteers
  * Sponsors
  * Host/s
  * Foodie/s

## Introduce the Teaching Team

  * Teaching Assistants
  * Teacher
  * hand off...
