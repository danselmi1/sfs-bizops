---
theme: gaia
backgroundColor: white
---
![bg fit](https://gitlab.com/sofreeus/sfs-bizops/-/raw/master/media/SFS%20Logo.png)

---
# Welcome and Purpose

* Welcome to the Software Freedom School.
* Our purpose here is to teach one another how to use and why to choose
  Free Software.

---
# Free Software Definition

Free Software is software that you are free to:
* **Use** for whatever purpose you see fit.
* **Study** how it works, for which access to the source code is a
  pre-requisite
* **Share** with your friends.
* **Modify** to better suit your needs.

---
# Free Software Definition

> In sum, Free Software authors respect your freedom to do science and
> art with the software. That seems like a good reason to prefer Free
> Software over non-free, proprietary software.

---
# Libre Not Gratis 

* Request For Payment / Pay What You Choose
* SFS class materials are free-libre like Free Software.

---
# Libre Not Gratis 

* SFS class deliveries are *not* free-gratis. Unlike like the pretzels
  in a bar.
* SFS does not give away class deliveries as a loss-leader to
  something else. 
* SFS only sells class deliveries. If it doesn't make money on class
  deliveries, it doesn't make money.

---
# Libre Not Gratis 

The ask for today's class is: $5

* You may pay the ask or 
* Pay What You Choose: more, less, or differently.

---
# Libre Not Gratis 

- Common ways to Pay What You Choose are:
- Pay with **crypto-currency**
- Send a **postcard** with a picture of you saving a whale or a kitten.
- **Teach** a class. This is our favorite, and you can make money at it!

---
# Practicalities 

* Restrooms -- Up stairs and to the right.
* Refreshments -- On the table
* Recycling -- In the hall outside

---
# Upcoming Events / We Take Requests

SFS has delivered several custom classes on request. If you or your
employer would like to commission a custom class, in-person, on-site, or
remote, just let us know. 

If it's Free Software, we teach it.

---
![bg fit](https://gitlab.com/sofreeus/sfs-bizops/-/raw/master/media/SFS%20Logo.png)

