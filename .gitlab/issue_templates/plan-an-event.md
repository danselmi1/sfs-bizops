#### Checklist

Pin down the Teacher, Topic, and Time.

This Issue's title may reference whichever part/s you are certain of.

Examples:
- "Plan a Jeffrey S. Haemer event" if you know the teacher (and the teacher is JSH)
- "Plan Godot" if you know the topic of the event (and it's Godot)
- "Plan 2/31" if you know the date of the event (and it's February 31st)

When you have teacher, topic, and time, create these tickets as needed:

- [ ] Create a "post" issue using the `post-an-event` template
- [ ] Create a "prep" issue using the `prep-an-event` template
- [ ] Create a "Heather's Kitchen" issue using the `pfood-an-event` template
- [ ] Create a "payout" issue using the `payout-an-event` template

##### Use this pattern for issue naming:

`<P-verb> <time (as short date)>: <topic> w <teacher>`

Example:

`Promote 4/20: ZickleTwister w Shoup`
