# SFS Event Calendar

See also: [The Past](schedule-past.md)

| Date          | Subject   | Location     | Teacher           | Prep | Post | Promote | Payout |
| ------------- | --------- | ------------ | ----------------- | ---- | ---- | ------- | ------ |
| 2021 week 49 | M is for ... |
| 2021-12-11 | Make | HQ and BBB | JSH | [x] | https://www.meetup.com/sofreeus/events/282539249/ | [x] | [ ] |
| 2021 week 48 | N is for ... |
| 2021 week 49 | O is for ... |
| ? | Observability | ? | ACW | [ ] | [ ] | [ ] | [ ] |
| 2021 week 50 | P is for ... |
| 2021 week 51 | Q is for ... |
| 2021 week 52 | R is for ... |
| 2022 week 01 | S is for ... |
| 2022 week 02 | T is for ... |
| 2022 week 03 | U is for ... |
| 2022 week 04 | V is for ... |
| 2022 week 05 | W is for ... |
| 2022 week 06 | X is for ... |
| 2022 week 07 | Y is for ... |
| 2022 week 08 | Z is for ... |
| ? | zShell | ? | GAR| [ ] | [ ] | [ ] | [ ] |

---

| ------------- | --------- | ------------ | ----------------- | ---- | ---- | ------- | ------ |
| ? | Games: Mindustry, iFrac, gnomine, +RasPi list  | hybrid | DLW| [ ] | [ ] | [ ] | [ ] |
| ? | Helm | - | ? | [ ] | [ ] | [ ] | [ ] |
