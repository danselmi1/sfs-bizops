# Prepare to Deliver an SFS Event

* [ ] Review the SFS Method and partner with an SFS Method SME (if wanted)
* [ ] Overview all materials so you have an idea of the whole class and how it achieves its Objectives in its time box.
* [ ] Ensure that the demo environment and the pair environment are identical.
* [ ] Ensure that labs are pair and share.
* [ ] Find a rubber duck or a partner. Do the following with your rubber duck or partner:
    - Rehearse labs.
    - Rehearse lectures.
* [ ] Verify screen sharing and video via BBB
