Welcome and Purpose

> Welcome to the Software Freedom School.
> Our purpose here is to teach one another how to use and why to choose Free Software.

Free Software Definition

> Free Software is software that you are free to:
> - **Use** for whatever purpose you see fit.
> - **Study** how it works, for which access to the source code is a pre-requisite
> - **Share** with your friends.
> - **Modify** to better suit your needs.

> In sum, Free Software authors respect your freedom to do science and art with the software.
> That seems like a good reason to prefer Free Software over non-free, proprietary software. So that we, and others like us, will have the ability and the freedom to grow from consumer to co-creator.

Libre Not Gratis - Request For Payment / Pay What You Choose

> SFS class materials are free-libre like Free Software.

> SFS class deliveries are *not* free-gratis like the pretzels in a bar. SFS does not give away class deliveries as a loss-leader to something else. SFS only sells class deliveries. If it doesn't make money on class deliveries, it doesn't make money.

> The ask for today's class is: ___ USD/BTC/whatever

> You may pay the ask or Pay What You Choose: more, less, or differently.

> Questions or payments: [pwyc@sofree.us](mailto:pwyc@sofree.us)

> Common ways to Pay What You Choose are:
> - Pay with **crypto-currency**
> - Send a **postcard** with a picture of you saving a whale or a kitten.
> - **Teach** a class. This is our favorite, and you can make money at it!

Restrooms, Refreshments, Recycling

Free Swag!

Upcoming Events / We Take Requests

> SFS has delivered several custom classes on request. If you or your employer would like to commission a custom class, in-person, on-site, or remote, just let us know. If it's Free Software, we teach it.

Introductions and Announcements

Who be you? Are you looking for work or workers?
