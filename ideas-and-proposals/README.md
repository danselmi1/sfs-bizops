### Talk Guidelines

A Gathering talk should be:
- About 22 minutes in length
- Teaches how to use and why to choose [Free Software](https://en.wikipedia.org/wiki/Free_and_open-source_software)
- Shares something you are...
    - Passionate About or...
    - Expert/experienced in


### Gathering Talk Ideas

| Title | Presenter | Suggested/Committed | Description |
| ---   | ---       | ---                 | ---         |
| _literally anything_ | Mike Shoup | Suggested | We'll take anything we can get. |
| Bash Traps | Aaron Brown | Committed | Simplify error handling in Bash scripts with the `trap` command. |
| Django | Mike Kelso | Suggested | Using the Python web framework |
| ~~Job Hunting~~ | ~~Troy Ridgley~~ | Given | ~~Top tricks for landing a new job~~ |
| Promotion Prep | Richarv Calvo | Suggested | How to line yourself up for a promotion at work |
| PiHole | Bryan Kobler | Suggested | Reducing bandwidth usage, distractions, and frustrations on your home internet |


* * * 

### Suggested Talk Ideas

- Perl basics (specifically because of Rich's grep questions. I think using PCRE's could be really useful/powerful, but I don't understand Perl very well at all).

- RegEx concepts... I think it'd be cool to have a small talk on things like special characters, control characters, look{ahead,around,behind}, etc. Just knowing the terminology would help people search the right things when they're trying to implement some language-specific RegEx. (Like I don't need to know the differences between JavaScript and PCRE implementations, just generically what a lookahead does.)

- How do you make architectural decisions? What factors in?

- Do another book report; the last one was awesome. :slightly_smiling_face:

- Your favorite feature in _ (you pick the technology/language/practice).

